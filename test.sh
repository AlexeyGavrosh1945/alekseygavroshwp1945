#!/bin/bash
set -e # exit on any error
mashinCode=$(cat mashin_code.txt) #path to pass connect pantheon
nameOrg="DrupalSquad"
number=$RANDOM
destinationSite="test"$RANDOM"gavrosh"
sourceSite="pmud8-mvp" 

function cloneSite {
if [ $# -ge 2 ]; then
	echo $1 $2 $3
	terminus site:clone $1 $2 $3
fi
}

chechAuth=$(terminus auth:whoami)


if [ $? -eq 0 ]; then
echo "Pantheon connected successfully"
else
	FILE="mashin_code.txt"
if [ -f $FILE ]; then
   mashinCode=$(cat mashin_code.txt)
else
   mashinCode="t2VuOjuxm-48xlO-IvqKB5uaPILgEELDia8a_DOLg-O5E"
fi
PATH="/home/user/terminus/vendor/bin:$PATH"
terminus auth:login --machine-token=$mashinCode
fi


if [ $? -eq 0 ]; then
	echo "good"
terminus site:create $destinationSite $destinationSite empty --org="$nameOrg"
cloneSite $sourceSite".dev" $destinationSite".dev" "--no-files --no-database --no-source-backup --no-destination-backup"
cloneSite $sourceSite".live" $destinationSite".dev" "--no-files --no-code --no-source-backup --no-destination-backup"

else
echo "ошибка подключения"
fi

read -r -p "Продолжить? Y or N " REPLY

if [[ $REPLY =~ ^[Yy]$ ]]
	then
		echo -e "\nХорошо"

	else
		echo -e "\n Вы отказались"

	fi





